---
layout: default
image: 42/card.png
languages: languages_42
urlprepend: "../../"
---

<p class="full" markdown="1">
![Devel Docs](../developers.svg)
</p>

# Developer Experience

### Updated HIG

The [GNOME Interface Guidelines](https://developer.gnome.org/hig/) are the primary source of design documentation for those creating software with the GNOME development platform. The HIG has been updated with up to date [Libadwaita](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/) widgetry and representative screenshots.

Most of the widget screenshots are generated from `.ui` files and thus become easier to maintain. And just like the platform itself, even the guidelines will follow your [dark style preference](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/styles-and-appearance.html#dark-style).

<p class="full" markdown="1">
![Devel Docs](../hig.svg)
</p>

### Builder

<picture>
	<source srcset="../builder-screenshot.webp" type="image/webp">
	<source srcset="../builder-screenshot.jpg" type="image/jpeg">
	<img src="scr-texteditor.jpg" alt="Builder Templates">
</picture>

Builder, GNOME's native IDE, ships with updated templates for creating new applications in Rust, Python and Vala. It also features a [better container workflow](https://thisweek.gnome.org/posts/2022/01/twig-28/#gnome-builder).

Builder now resolves files to a *toolbox* or *podman* container correctly and therefore can offer *clang* completions, symbol resolution and hover information from the container.

### Libadwaita

Libadwaita is a GTK 4 library implementing the GNOME HIG, complementing GTK.
It is a direct successor to Libhandy, which tried to fill this role for GTK 3.
Libadwaita 1.0 was [released](https://blogs.gnome.org/alexm/2021/12/31/libadwaita-1-0/) at the end of 2021.

Many of the GNOME applications that are being ported to GTK 4 are using libadwaita to provide a consistent
style and user experience.

The 1.0 release includes reworked [documentation](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/) and new widgets such as [Toasts](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/main/class.Toast.html).

<picture>
	<source srcset="../libadwaita-toast-screenshot.webp" type="image/webp">
	<source srcset="../libadwaita-toast-screenshot.png" type="image/png">
	<img src="../libadwaita-toast-screenshot.png" alt="Libadwaita Toasts">
</picture>

### Sysprof

The development version of Sysprof can find debug-info for Flatpak apps.
Combined with the introduction of frame pointers to org.freedesktop.Platform 21.08 and to the nightly
org.gnome.Platform, you can now use Sysprof to profile GNOME Flatpak apps.

This also applies when using Sysprof as part of GNOME Builder.

### Tracker

Tracker, GNOME's search and indexing backend, has undergone extensive development during this cycle.

* There is new API to prepare statements from queries stored in GResources, encouraging further separation of code and queries.
* Namespace manager and statement APIs now work on HTTP connections.
* There is new API to dump database contents into textual RDF formats.
* There is a new flag to enable SPARQL/RDF compliant behavior of blank nodes.
* There is new API to map connections as services for other connections privately to a process.

#### Tracker Miners

* There are major performance improvements, plus a smaller memory footprint.
* There is support for fanotify file monitoring, in the kernel versions and filesystems that apply.
* The identifiers assigned to file content and metadata are now stable across database resets.
* The filesystem miner changed to skip mountpoint folders found during recursive indexing. Mountpoints should be configured as indexed folders separately.

### GJS

* GJS now now uses SpiderMonkey 91, bringing lots of modern JavaScript conveniences
* GObject interfaces are now enumerable
* Type safety has been improved with a large refactoring
* Many new operators and methods have been added
* Pressing <kbd>Ctrl</kbd>+<kbd>D</kbd> at the prompt now quits the debugger
* Syntax errors include column numbers

### Vala

The new 0.56 release includes lots of new features:
- asynchronous main function
- nested functions
- redesigned error and warning output
- dynamic invocation of signals
- partial classes
- length-type for arrays
- foreach support on `Glib.Sequence` and `Glib.Array`
- several new bindings
- Vala SDK Extension on Flathub

For more information see the Vala [release notes](http://lw64.gitlab.io/vala/2022/03/18/Vala-0.56).

### Documentation

[Developer.gnome.org](http://developer.gnome.org) has been updated during this cycle:
- a new Getting Started tutorial, to help newcomers to the GNOME development platform and its tools
- a series of short tutorials on UI elements, bridging the gap between the Interface Guidelines and the API references for GTK and Libadwaita
- more content ported from the old Developer documentation website

 Many libraries had their docs ported to gi-docgen, among others:

* [Libsecret](https://gnome.pages.gitlab.gnome.org/libsecret/)
* [Libhandy](https://gnome.pages.gitlab.gnome.org/libhandy/)
* [Libpeas](https://gnome.pages.gitlab.gnome.org/libpeas/libpeas-1.0/)
* [Gtksourceview5](https://gnome.pages.gitlab.gnome.org/gtksourceview/gtksourceview5/)
