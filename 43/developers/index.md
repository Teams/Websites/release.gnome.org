---
layout: default
image: 43.jpg
urlprepend: "../../"
languages: languages_42
---

<p class="full" markdown="1">
![Devel Docs](developers.svg)
</p>

# GNOME 43 for Developers

GNOME's developer technologies have had a host of improvements for GNOME 43. The new version includes a huge Builder update, plus a range of enhancements to developer libraries, including GTK, GLib, libadwaita, and more.

## Builder

<picture>
	<source srcset="builder-screenshot.webp" type="image/webp">
	<source srcset="builder-screenshot.png" type="image/png">
	<img src="builder-screenshot.png" alt="Software">
</picture>

Builder has had a very significant update for GNOME 43. Freshly ported to GTK 4, the new version includes a host of user interface enhancements and improvements.

### Interface Changes

Along with being ported to GTK 4, Builder has had a major UI overhaul for GNOME 43. The app's UI has been reimagined, refined, and refreshed.

 * Builder now has tabs! This is the default interface for your open documents and terminals, and provides a familiar way to navigate. Tabs can be organised into different frames, to allow documents and terminals to be viewed side by side. Builder's previous UI for switching between open documents and terminals is still available for those who want to use it: see the "Document Navigation" option in Builder's preferences.
 * The status bar is another new addition for version 43. This displays the Git branch, the number of warnings and errors, which language the current document is set to, and other useful information.
 * Builder's panels can now be reorganized to create custom layouts. Drag each panel's tabs to move them between panels, or to keep more sections visible at the same time.
 * With the switch to GTK 4, Builder has a refreshed UI style, in line with other modern GNOME apps. Furthermore, Builder's interface colors now follow the color scheme used for code highlighting. In the Preferences, change the Color preference under Appearance to select your preferred highlighting scheme, and the rest of Builder's UI will update to match.
* A new command editor makes it possible to create custom run commands, which can be added to the build pipeline, or used with the debugger, profiler, or other tooling. To create a command, see the Commands section in the Preferences.
* Finally, Builder's Preferences have been redesigned for GNOME 43, with an all new and improved layout, and easier navigation.

### New Capabilities  

The changes in the new Builder release are more than skin deep, and the new version includes a number of important functional enhancements.

* Version 43 includes a new, more powerful set of options for running apps. This includes new options to run apps with different accessibility and internationalization settings, such as with high contrast mode,  or with a right-to-left layout. There are also new options for detecting memory leaks. 
* Profiling Flatpak applications is now more precise and includes a greater range of instruments. These improvements make use of new GNOME SDK integration, which runs Sysprof within build and runtime container environments.
* Builder's language server support has been rewritten for 43 to improve robustness. Code highlighting and error checking has been extended to cover Bash, Java, and the [Blueprint markup language](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/). Support has also been improved for Go and PHP.

## GTK

![GTK 4](gtk.svg){: .fullimg}

GNOME 43 is accompanied by GTK 4.8, which was released earlier in the month. This new version of the toolkit includes a range of new features and enhancements.

 * GTK now supports high-resolution scroll devices, on both X11 and Wayland.
 * A new widget called [`GtkInscription`](https://docs.gtk.org/gtk4/class.Inscription.html) can be used to lay out text by sizing it according to the available space, as opposed to sizing according to the text itself. This is primarily useful for laying out text within lists.
 * [`GtkPicture:content-fit`](https://docs.gtk.org/gtk4/class.Picture.html#sizing-the-paintable) provides better control for how images are scaled and cropped to the available space. Options for fitting images include [fill, contain, cover, and scale-down](https://docs.gtk.org/gtk4/enum.ContentFit.html). These replace the previous `keep-aspect-ratio` option, which has been deprecated.
 * `GtkListView` and `GtkColumnView` scrolling is now smoother, thanks to ongoing optimization work.
 * It is now possible to set fractional letter spacing using CSS. 
 * The GTK font chooser dialog is now able to handle a greater range OpenType features. More information about this feature is included in [Matthias Clasen's blog post on the subject](https://blog.gtk.org/2022/09/19/inside-the-gtk-font-chooser/).
 * A color picker is now available on Windows.
 * Also on Windows, smooth scrolling has been restored.

## GLib

GNOME 43 is accompanied by GLib 2.74, which includes a set of improvements:

* `G_DEFINE_ENUM_VALUE`, `G_DEFINE_ENUM_TYPE`, and `G_DEFINE_FLAGS_TYPE` are new macros which allow defining enumeration and flags types directly in C code, without the need to use `glib-mkenums`. This can be more convenient when dealing with small numbers of enumerations.
* GLib has been ported from libprce to libprce2. libpcre2 can behave differently from libpcre, so it is recommended to review all uses of the GRegex API.
* `GListStore` now has the `GListStore:n-items` property, to make it easier to update UI elements when a list store is empty.
* New `g_idle_add_once()` and `g_timeout_add_once()` functions make it simpler to add an idle or timeout callback which is only ever dispatched once.
* Automatic NULL-termination support has been added to `GPtrArray`.

Note that GLib 2.74 will require an additional part of the C99 specification: variadic arguments in macros, using `__VA_ARGS__`. All supported toolchains (GCC, Clang, MSVC) already comply to this standard. Other compilers should be checked that they support C99.

## Libadwaita

GNOME 43 is accompanied by libadwaita 1.2.0. The new version includes a number of new widgets:

* [`AdwEntryRow`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.EntryRow.html): a new type of boxed list row, which includes an editable text field. This is now the recommended way to include text fields within boxed lists, and has the advantage of being adaptive. `AdwPasswordEntryRow` is also available for entries which include sensitive information.
* [`AdwMessageDialog`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.EntryRow.html): an alternative to `GtkMessageDialog`, which adapts to the screen size, and has an easier to use API.
* [`AdwAboutWindow`](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.AboutWindow.html): a new about window, which can be used as an alternative to `GtkAboutDialog`.

Other smaller changes in libadwaita 1.2.0 include:

* New API to control `AdwTabBar` shortcuts.
* `AdwPropertyAnimationTarget`: a way to easily animate a specific property on an object.
* New `AdwToast` capabilities, including the ability to use a custom widget as the title.

More detailed information can be found in [Alexander Mikhaylenko's blog post on libadwaita 1.2](https://blogs.gnome.org/alexm/2022/09/15/libadwaita-1-2/).

## GNOME JavaScript

GNOME 43 is accompanied by GJS 1.74.0, which includes

* Many improvements to [the examples](https://gitlab.gnome.org/GNOME/gjs/-/tree/master/examples) and [documentation](https://gitlab.gnome.org/GNOME/gjs/-/blob/master/doc/README.md).
* It's now possible to use [`GObject.BindingGroup.prototype.bind_full()`](https://docs.gtk.org/gobject/method.BindingGroup.bind_full.html).
* It's now possible to use [`Gio.ActionMap.prototype.add_action_entries()`](https://docs.gtk.org/gio/method.ActionMap.add_action_entries.html).
* DBus proxy classes now include methods named with the suffix 'Async', which perform async calls to DBus APIs and return Promises. This is in addition to the existing suffixes 'Sync' (for blocking calls) and 'Remote' (for async calls with callbacks.)
* [`Gio.FileEnumerator`](https://docs.gtk.org/gio/class.FileEnumerator.html) is now iterable, both synchronously (with for-of or array spread syntax) and asynchronously (with for-await-of).
* The interactive interpreter now displays its output more intelligently, pretty-printing the properties and values of objects based on their type. This improvement also applies to the `log()` and `logError()` functions.
* New JavaScript features! This version of GJS is based on SpiderMonkey 102, an upgrade from the previous ESR (Extended Support Release) of SpiderMonkey 91. Here are the highlights of the new JavaScript features.
	* The [`Object.hasOwn()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/hasOwn) static method can be used as an easier replacement for `Object.prototype.hasOwnProperty.call(...)`.
	* [`Intl.supportedValuesOf()`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl/supportedValuesOf) lets you enumerate which calendars, currencies, collation strategies, numbering systems, time zones, and units are available for internationalization.
* Several optimizations and bug fixes check the [GJS release notes](https://gitlab.gnome.org/GNOME/gjs/-/blob/master/NEWS)

## Developer Docs

![Getting Started](getting-started.svg){: .fullimg}

GNOME's developer documentation has received a number of improvements since the last release.

 * [Developer documentation pages](https://developer.gnome.org/documentation/) now have a link to let you easily view and edit the source text in GitLab.
 * Tutorials on [drag and drop](https://developer.gnome.org/documentation/tutorials/drag-and-drop.html) and [widget templates](https://developer.gnome.org/documentation/tutorials/widget-templates.html) have been added.
 * The [getting started guide](https://developer.gnome.org/documentation/tutorials/beginners/getting_started.html) has been ported to Vala.

Finally, Vala was given a [new website](https://vala.dev/)!

