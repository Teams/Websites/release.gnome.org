---
layout: default
image: 43.jpg
languages: languages_43
urlprepend: "../"
---

# Introducing GNOME&nbsp;43, "Guadalajara"

**September 21, 2022**

After 6 months of hard work, the GNOME project is proud to present version 43. This latest GNOME release comes with improvements across the board, ranging from a new quick settings menu, a redesigned Files app, and hardware security integration. GNOME 43 continues the trend of GNOME apps migrating from GTK 3 to GTK 4, and includes many other smaller enhancements.

GNOME 43 is code-named "Guadalajara", in recognition of the work done by the organizers of GUADEC 2022.

## Quick Settings

GNOME 43 comes with a redesigned system status menu, which allows quickly changing commonly used settings. Settings which previously required digging into menus can now be changed with the click of button. The new design also makes it easy to see the status of your settings at a glance.

In addition to making existing settings easier to use, the new settings menu includes some notable new functionality:

* A setting for the UI style is included in the menu for the first time, to allow switching between light and dark styles. This setting was previously only accessible from the Settings app.
* A new screenshot button is included, which complements the built-in screenshot functionality that was introduced in GNOME 42.
* When multiple sound devices are present, it is now possible to switch between them from the menu, avoiding the need to dig into the Settings app.
* When VPN is turned off, pressing the VPN button will connect to the last used network. 

<video controls muted class="rounded">
  <source src="quicksettings43.webm" type="video/webm">
  <source src="quicksettings43.mp4" type="video/mp4">
</video>

The new system status menu provides all the same features as the previous design, including the ability to control GNOME's networking features, such as Bluetooth and USB tethers, and Wi-Fi hotspots.

## Next Generation Toolkit

GTK 4 is the major new version of GNOME's user interface toolkit, which was released in December 2020. With the new version, apps get faster, smoother graphics, new interface widgets, and a fresh new look.

The previous GNOME release, version 42, saw a number of GNOME apps adopt GTK 4. With GNOME 43, this trend continues, with more apps switching from GTK 3 to GTK 4. This includes:

- [![Files](https://apps.gnome.org/icons/scalable/org.gnome.Nautilus.svg){: .icon-dropshadow} <br>Files](https://apps.gnome.org/app/org.gnome.Nautilus/)
- [![Maps](https://apps.gnome.org/icons/scalable/org.gnome.Maps.svg){: .icon-dropshadow} <br>Maps](https://apps.gnome.org/app/org.gnome.Maps/)
- [![Logs](https://apps.gnome.org/icons/scalable/org.gnome.Logs.svg){: .icon-dropshadow} <br>Logs](https://apps.gnome.org/app/org.gnome.Logs/)
- [![Builder](https://apps.gnome.org/icons/scalable/org.gnome.Builder.svg){: .icon-dropshadow} <br>Builder](https://apps.gnome.org/app/org.gnome.Builder/)
- [![Console](https://apps.gnome.org/icons/scalable/org.gnome.Console.svg){: .icon-dropshadow} <br>Console](https://apps.gnome.org/app/org.gnome.Console/)
- ![Initial Setup](eek.notanapp.InitialSetup.svg){: .icon-dropshadow} <br>Initial Setup
- ![Parental Controls](org.gnome.ParentalControls.svg){: .icon-dropshadow} <br>Parental Controls
{: .icon-grid.small}

## Files Refresh

As part of the transition to GTK 4, the Files app has had numerous updates.

* File and folder properties windows have been given a new, modern design, which gives a better overview of each item. They also include new features, like a button to open the parent folder.
* The app is now adaptive, meaning that it can be used at narrow widths, and that it will automatically adjust its layout as its windows are resized.
* Menus have been reorganized, so they are more logical and easier to use.
* The lists of search results, recent, and starred files, have a new layout, with a better indication of the location of each file.
* The new *Open With* dialog makes it easy to control which apps should be used to open different types of file.
* In list view, opening the context menu for the current directory is now much much easier, using the gutters at each side of the list. 

<picture>
	<source srcset="nautilus-screenshot.webp" type="image/webp">
	<source srcset="nautilus-screenshot.png" type="image/png">
	<img src="nautilus-screenshot.png" alt="Files">
</picture>

## Calendar, Contacts, Calls

The Calendar, Contacts and Calls apps have all been enhanced for GNOME 43.

* Calendar has an updated interface, with a new sidebar that includes a navigation calendar and a list of upcoming events. The calendar grid has also been given a fresh coat of paint, with a new color palette.
* Contacts in GNOME 43 now allows importing and exporting contacts as vCard files.
* Calls has a collection of improvements, including faster start times, support for encrypted VOIP calls, and the ability to send SMS messages from the call history.

## Device Security Settings

GNOME's privacy settings include a new Device Security page for version 43. This provides information about the security of your physical hardware, its configuration, and its firmware.

The Device Security settings can be used to detect a variety of hardware security issues, including manufacturing errors and hardware misconfiguration. The new settings can also warn about potential security issues as they happen, such as physical device tampering, or sudden degradation of security tests, which could indicate the presence of malware.

With the amount of malware that targets firmware on the rise, device security is an area of growing importance, where further work is planned for the future.

## Web Apps

GNOME 43 includes the ability to install websites to the desktop, as apps. This functionality is a new and improved version of the web apps feature that was available in previous versions of GNOME.

In Web, websites can be installed as apps, using the "Install Site as Web Application" menu item. A selection of web apps is also included in Software: when browsing the categories of apps, look out for the "Picks from the Web" sections.

In both cases, when a web app is installed, an app launcher is added to the Activities Overview, just like with regular apps. Web apps also launch in their own dedicated windows. Where possible, offline functionality is supported, so some web apps can be used even when you aren't connected to the internet.

Web apps can be removed in the usual way from Software, and Web also includes its own page for web app management.

## That's Not All

GNOME 43 includes many other small enhancements, including:

* The screen keyboard now shows suggestions as you type. It will also show Ctrl, Alt, and Tab keys when typing in a terminal.
* Web's screenshot feature is now easier to use: it can now be found in the web page context menu, or triggered with the Shift+Ctrl+S keyboard shortcut.
* Also in Web, the style of interface elements in web pages has also been updated, to match modern GNOME applications.
* The Characters app now includes a much wider selection of emoji, including people with different skin tones, genders, and hair styles, and more regional flags.
* Some of the animations in the Activities Overview have been optimized, so that they are smoother.
* The "about windows" of GNOME apps, which show details about each app, have been revamped.
* In Software, application pages have an improved switcher for selecting the source and format.
* The dark UI style used by GTK 4 apps has been polished, so the appearance of bars and lists is more harmonious.
* When connecting to GNOME with a remote desktop app (using RDP), it is now possible to receive audio from the host.
* GNOME's range of alert sounds has been updated, and includes a new default alert sound.

<picture>
	<source srcset="software-screenshot.webp" type="image/webp">
	<source srcset="software-screenshot.png" type="image/png">
	<img src="software-screenshot.png" alt="Software">
</picture>

## Developer Experience

GNOME 43 includes new features and improvements for developers who are using the GNOME platform. Read the developers section to learn more.

<ul class="linkdiv">
  <li>
    <a href="developers/index.html"><span class="title">What's New for Developers</span>
    <span class="description">New features for those working with GNOME technologies.</span>
    </a>
  </li>
</ul>

## Getting GNOME 43

GNOME’s software is [Free Software](https://gnu.org/philosophy/free-sw.html): all [our code](https://gitlab.gnome.org/GNOME) is available for download and can be freely modified and redistributed according to the respective licenses. To install it, we recommend that you wait for the official packages provided by your vendor or distribution. Popular distributions will make GNOME 43 available very soon, and some already have development versions that include the new GNOME release. You can also try the [GNOME OS image](https://os.gnome.org/) as a virtual machine, using     the [Boxes](https://apps.gnome.org/app/org.gnome.Boxes/) app.

## About GNOME

[The GNOME Project](https://www.gnome.org/about/) is an international community supported by a non-profit Foundation. We focus on user experience excellence and first-class internationalization and accessibility. GNOME is a free and open project: if you want to join us, [you can](https://welcome.gnome.org).
