---
layout: default
image: 44.jpg
languages: languages_44
urlprepend: "../"
---

# Introducing GNOME&nbsp;44, “Kuala Lumpur”

**March 22, 2023**

After 6 months of hard work, the GNOME project is proud to present GNOME version 44. This latest release includes substantial improvements, with new features, enhancements, and lots of fixes. Highlights include major improvements to the Settings app, a better quick settings menu, and a streamlined Software app. More details are included in the release notes below.

GNOME 44 is code-named “Kuala Lumpur”, in recognition of the work done by the organizers of GNOME.Asia 2022.

## File Chooser Grid View

<picture>
	<source srcset="file-chooser-screenshot.png" type="image/png">
	<img src="file-chooser-screenshot.png" alt="File chooser grid view screenshot">
</picture>

GNOME's file chooser dialogs have only ever had a list view, which is great when you want to pick a file based on its name, but isn't so good when picking files based on their thumbnails. Over the years, GNOME users have therefore repeatedly requested that a grid view be added to the file chooser.

With GNOME 44, that request has finally been answered. This has been one of the most positively received changes in our history, so we are confident that people will like it!

The new grid view is available in file choosers that use GTK4. Some apps may use the older GTK3 version of the file chooser, which does not include the new grid view.

## Updated Settings Panels

GNOME 44 is a big release for the GNOME Settings app, with four settings panels having been reworked for the new version.

### Device Security

<picture>
	<source srcset="device-security-screenshot.png" type="image/png">
	<img src="device-security-screenshot.png" alt="Device Security settings">
</picture>

Device Security was introduced to the Settings app last release, in GNOME 43. Since then, they have received a major round of updates.

The new version shows the device's security status as a description, such as "Checks Failed", "Checks Passed", or "Protected". This makes the panel easier to understand.

For those who want to dig into the technical details, or who want to report an issue about the security of their device, a new feature generates a full device security report for the device. This provides an overview of what is happening under the hood, and can be easily copied into issue reports and support requests.

### Accessibility

<picture>
	<source srcset="accessibility-screenshot.png" type="image/png">
	<img src="accessibility-screenshot.png" alt="Accessibility settings">
</picture>

GNOME's accessibility settings have been redesigned for GNOME 44. The different sections of settings are now split up, to make them easier to navigate. The design of individual settings has also been improved, to be clearer and more consistent with the rest of settings. Additionally, many settings have had descriptions added.

The new accessibility settings also include some new features:

 * An over-amplification setting has been added, to increase the volume above the usual maximum threshold.
 * Under *Typing*, an option has been added to enable accessibility features using the keyboard.
 * There is now a test area for the cursor blinking setting.
 * A new setting to make scrollbars be always visible has been added to the *Seeing* section.

### Sound

<picture>
	<source srcset="sound-screenshot.png" type="image/png">
	<img src="sound-screenshot.png" alt="Sound settings">
</picture>

GNOME's sound settings have also had an upgrade since last release, which overall makes them much easier to use:

* Volume level control has been moved into a separate window, making the more commonly used output and input controls easier to access.
* It's now possible to disable the alert sound, and a new alert sound window makes it easy to browse available sounds.
* The sound test window has been redesigned, eliminating previous scaling issues when there are many outputs, and providing a more attractive interface.

The sound settings also have a number of other polish improvements, including new volume level indicators, and better presentation of missing devices.

### Mouse & Touchpad

<picture>
	<source srcset="mouse-and-touchpad-screenshot.png" type="image/png">
	<img src="mouse-and-touchpad-screenshot.png" alt="Mouse and Touchpad settings">
</picture>

The final settings panel to have been reworked for GNOME 44 is Mouse & Touchpad. The most noticeable change is the addition of videos which demonstrate the different available options. Not only do these look great, but they also make the various settings easier to understand.

Other improvements in the Mouse & Touchpad settings include a new testing window, a new Mouse Acceleration setting, and tick marks in the different sliders.
   
## Enhanced Quick Settings

The quick settings menu was a new feature last release, which has subsequently been improved:

* The Bluetooth quick settings button now has a menu. This shows which devices are connected, as well as allowing devices to be connected and disconnected.
* The menu now lists apps which have been detected to be running without an open window. This feature makes it possible to check if a particular app is running in the background. Currently only Flatpak apps are included in the background apps list.
* Descriptions have been added to each of the quick settings toggle buttons. These show more of the status of each setting.

## Streamlined Software

Software in GNOME 44 offers a smoother and faster experience. The pages for each software category are now displayed more quickly, so you can browse with less interruption. Reloading of pages has also been reduced.

The app also has improved support for next-generation software formats: Flatpak runtimes are now automatically removed when not needed, in order to save on disk space, and image-based operating system updates now have both progress information and descriptions.

Finally, Software version 44 includes a collection of UI enhancements, including better-looking reviews and error messages.

## Files Improvements

Files comes with a collection of improvements for GNOME 44:  

* When Files was converted to GTK4, it lost its expand folders in list view option. With GNOME 44, that option is now back. When enabled from the preferences, it makes it possible to view the contents of a folder without descending into it, which can be particularly useful for quickly exploring nested folders.
* Tabs now have additional options, including the ability to move tabs to new windows. It is now also possible to drag items on to a tab.
* Finally, the number of grid view sizes has been increased.

## Even More Settings Improvements

In addition to the numerous reworked settings panels in GNOME 44, there is also a good collection of smaller Settings improvements. These include:

* In the *Wi-Fi* settings, it is now possible to share a Wi-Fi password using a QR code.
* The *About* section now includes the kernel and firmware versions.
* The *Thunderbolt* settings will now only be shown when there is Thunderbolt hardware.
* In the *Network* settings, it is now possible to add and configure [Wireguard VPNs](https://www.wireguard.com/).

## New Circle Members

[GNOME Circle](https://circle.gnome.org/) is a fantastic collection of apps that are developed as part of the GNOME Project. Since GNOME 43 was released, ten new apps have joined:

- [![Zap](zap.svg){: .icon-dropshadow} <br>Zap](https://gitlab.com/rmnvgr/zap/)
- [![Boatswain](https://apps.gnome.org/icons/scalable/com.feaneron.Boatswain.svg){: .icon-dropshadow} <br>Boatswain](https://apps.gnome.org/app/com.feaneron.Boatswain)
- [![Emblem](https://apps.gnome.org/icons/scalable/org.gnome.design.Emblem.svg){: .icon-dropshadow} <br>Emblem](https://apps.gnome.org/app/org.gnome.design.Emblem/)
- [![Lorem](https://apps.gnome.org/icons/scalable/org.gnome.design.Lorem.svg){: .icon-dropshadow} <br>Lorem](https://apps.gnome.org/app/org.gnome.design.Lorem)
- [![Workbench](https://apps.gnome.org/icons/scalable/re.sonny.Workbench.svg){: .icon-dropshadow} <br>Workbench](https://apps.gnome.org/app/re.sonny.Workbench)
- [![Komikku](https://apps.gnome.org/icons/scalable/info.febvre.Komikku.svg){: .icon-dropshadow} <br>Komikku](https://apps.gnome.org/en/app/info.febvre.Komikku/)
- [![Chess Clock](https://apps.gnome.org/icons/scalable/com.clarahobbs.chessclock.svg){: .icon-dropshadow} <br>Chess Clock](https://apps.gnome.org/app/com.clarahobbs.chessclock/)
- [![Eyedropper](https://apps.gnome.org/icons/scalable/com.github.finefindus.eyedropper.svg){: .icon-dropshadow} <br>Eyedropper](https://apps.gnome.org/app/com.github.finefindus.eyedropper/)
- [![Elastic](https://apps.gnome.org/icons/scalable/app.drey.Elastic.svg){: .icon-dropshadow} <br>Elastic](https://apps.gnome.org/en-GB/app/app.drey.Elastic/)
- [![Clairvoyant](https://apps.gnome.org/icons/scalable/com.github.cassidyjames.clairvoyant.svg){: .icon-dropshadow} <br>Clairvoyant](https://apps.gnome.org/app/com.github.cassidyjames.clairvoyant/)
{: .icon-grid}

Welcome to GNOME!

## And That's Not All…

There are many other smaller improvements in GNOME 44, including:

* GNOME's low battery power notifications have been reworked, with new icons and updated text.
* In Contacts, it is now possible to share a contact using a QR code.
* Web, the GNOME browser, has been converted to GTK4. This is part of the ongoing effort to convert GNOME applications to the latest version of GTK, which delivers performance and user experience improvements.
* Search results from settings can now be disabled.
* Web has great new popovers for saving passwords. These replace the info bars that were used previously.
* In Maps, more places have pictures, thanks to fetching images from both Wikidata and Wikipedia. Maps also includes a collection of smaller improvements, including the ability to navigate search results using the keyboard.
* Drag and drop in the app grid has been improved.
* Console, the terminal app, now has a tab overview option, to display open tabs in a grid.
* Weather now has a smooth temperature chart, and a restyled header bar.
* Various missing keyboard shortcuts have been added to Contacts, including `Ctrl+F`, `Ctrl+,`, and `Ctrl+Return`. It also has various other bug fixes.
* GNOME's collection of wallpapers includes four fantastic new backgrounds 

<picture>
	<source srcset="wallpapers.png" type="image/png">
	<img src="wallpapers.png" alt="New wallpapers">
</picture>

## Developer Experience

GNOME 44 includes new features and improvements for developers who are using the GNOME platform. Read the developers section to learn more.

<ul class="linkdiv">
  <li>
    <a href="developers/index.html"><span class="title">What's New for Developers</span>
    <span class="description">New features for those working with GNOME technologies.</span>
    </a>
  </li>
</ul>

## Getting GNOME 44

GNOME’s software is [Free Software](https://gnu.org/philosophy/free-sw.html): all [our code](https://gitlab.gnome.org/GNOME) is available for download and can be freely modified and redistributed according to the respective licenses. To install it, we recommend that you wait for the official packages provided by your vendor or distribution. Popular distributions will make GNOME 44 available very soon, and some already have development versions that include the new GNOME release. You can also try the [GNOME OS image](https://os.gnome.org/) as a virtual machine, using the [Boxes](https://apps.gnome.org/app/org.gnome.Boxes/) app.

## About GNOME

[The GNOME Project](https://www.gnome.org/about/) is an international community supported by a non-profit Foundation. We focus on user experience excellence and first-class internationalization and accessibility. GNOME is a free and open project: if you want to join us, [you can](https://welcome.gnome.org).

