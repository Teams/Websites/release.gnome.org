---
layout: default
image: 45.jpg
urlprepend: "../../"
---

<p class="full" markdown="1">
![Devel Docs](developers.svg)
</p>

# What's new for developers

GNOME 45 is packed with new features and enhancements for those who use the GNOME platform. These include updates to GNOME's developer tools, improved libraries, and updated language bindings.

## GNOME Shell Extensions

**GNOME Shell 45 includes technical changes which are incompatible with all existing GNOME shell extensions. All shell extensions therefore need to be updated to work with GNOME 45.**

In GNOME 45, GNOME Shell has switched from GJS's import system to the standard JavaScript import system. This change will improve compatibility with the broader JavaScript ecosystem, and ensures that GNOME is following JavaScript standards. However, it also means that shell extensions written for earlier versions of GNOME Shell will be incompatible with version 45 and later.

Extension developers are encouraged to update their extensions for GNOME 45 prior to it being included in stable distro releases. Users are also encouraged to contact the developers of their favorite extensions, to make them aware of the change.

GNOME OS and distros which include GNOME development releases (such as Fedora) can be used for extension testing and development. Extension developers can also get live support in the [GNOME  Extensions Matrix channel](https://matrix.to/#/#extensions:gnome.org).

A more extensive description of this change is available on the [GNOME Shell Development Blog](https://blogs.gnome.org/shell-dev/2023/09/02/extensions-in-gnome-45/).

## Developer Tools

GNOME 45 comes with major updates to the project's developer tools.

### GNOME Builder

In GNOME 45, Builder plugins can now be written using modern JavaScript (thanks to the platform provided by GJS). JavaScript offers a great option for those wanting to write plugins in a dynamic language, and supplements the compiled languages (C, C++, Rust, and Vala) which can already be used for writing plugins. If you want to create a JavaScript plugin for Builder, an [example is available](https://gitlab.gnome.org/GNOME/gnome-builder/-/tree/main/src/plugins/examples/gjs?ref_type=heads). Those who are new to using GObject via JavaScript can find guidance in the [GJS documentation](https://gjs.guide/guides/).

Builder 45 also includes support for many of the new Sysprof features that are part of GNOME 45 (see below for more details).

Other enhancements in version 45 include:
 
 * Improved support for LSP, to provide a better out of the box experience with different projects. This includes improvements for projects which use Meson, Swift, PHP Intelephense, and SourceKit.
 * Updated color schemes to align with modern GNOME UI style.
 * The Valgrind plugin has gained the option to set the number of callers to record in stack traces.
 * Ctrl+Q will now unload workbenches before exiting to ensure that session state can be saved.
 * Improved pattern matching in the symbol tree popover and global search.

### Sysprof

Sysprof has been redesigned and rewritten from the ground up for GNOME 45. The new version comes with GTK 4 integration, more advanced features, and a new modern app design. Under the hood, highlights include:

* `libsysprof` has been rewritten around a new document model for captures. This has allowed many performance improvements as well as simplifying Sysprof's user interface code. 
* A new `SysprofProfiler` API that is simpler than its predecessor and allows for easier implementation of instruments in a race-free manner.
* More data types in `libsysprof-capture`.
* `sysprof-cli` and `sysprof-agent` have both been updated to use the new `libsysprof` profiler interface, and support new capture features.
* Improved support for Flatpak, Rust, and C++.

The Sysprof app has also been completely redesigned. A new greeter makes it easy to configure a capture before it is started. Capture results can be viewed in many different ways, including viewing the stack trace over time, a log view, a marks view for viewing annotations, a process view, a flame graph, and much more.

Christian Hergert has written a series of blog post on these changes, which are worth checking out:

* [GListModel as a data format](https://blogs.gnome.org/chergert/2023/07/18/glistmodel-as-a-data-format/.)
* [How to use Sysprof again](https://blogs.gnome.org/chergert/2023/07/28/how-to-use-sysprof-again/)
* [More sysprofing](https://blogs.gnome.org/chergert/2023/08/04/more-sysprofing/)
* [Sysprof 45](https://blogs.gnome.org/chergert/2023/07/27/sysprof-45/)

### Workbench

[Workbench](https://apps.gnome.org/Workbench/) is a fantastic app for learning and prototyping with GNOME technologies. With it, you can edit GTK and CSS and view the resulting UI in real time. Workbench comes with a library of over a hundred high-quality examples, and  supports coding in JavaScript, Rust and Vala. 

Changes in its latest release include:

* Ability to have multiple windows/sessions open at the same time.
* Projects can now be written in Rust.
* A new offline documentation viewer, called "Manuals", has been added.
* The default syntax for UI has been changed to [Blueprint](https://jwestman.pages.gitlab.gnome.org/blueprint-compiler/) (XML is still available).
* Improved CSS linting via `GTKCssLanguageServer`.
* 50 new examples in the library.

Workbench is part of [GNOME Circle](https://circle.gnome.org/) and can be [downloaded from Flathub](https://flathub.org/apps/re.sonny.Workbench).

## GNOME Libraries

GNOME's development libraries also come with new features and enhancements in the latest version.

### Libadwaita

GNOME 45 is accompanied by libadwaita 1.4. The new version comes with many new capabilities and enhancements.

#### New adaptive APIs and widget styles

The new libadwaita release brings a new set of APIs for adaptive behavior, along with associated style changes to several common widgets.

The main change for adaptive behavior is the introduction of [AdwBreakpoint](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Breakpoint.html), which provides a way to specify at which sizes different UI layouts should be used. Breakpoints are more robust and provide a higher quality user experience than libadwaita's pre-existing adaptive widgets. They also allow different UI layouts at different window sizes. For example, when a window is narrow, header bar controls can be moved to a new toolbar which pops up at the bottom of the view.

As a result of this change, [AdwFlap](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Flap.html), [AdwSqueezer](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Squeezer.html), [AdwLeaflet](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.Leaflet.html), and [AdwViewSwitcherTitle](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.ViewSwitcherTitle.html) are all deprecated in the new release. In some cases, these can be replaced by breakpoints alone. In other cases, new replacement widgets can be used, in the shape of [AdwNavigationView](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.OverlaySplitView.html), [AdwNavigationSplitView](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.NavigationSplitView.html), and [AdwOverlaySplitView](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.OverlaySplitView.html).

Several style updates are also part of these changes:
[AdwNavigationSplitView](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.NavigationSplitView.html) brings a cool new full-height look for sidebars, and [AdwToolbarView](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.ToolbarView.html) is a new widget which provides a new style for header bars.

Many core GNOME apps are already using the new adaptive APIs, and can be used for reference. A [migration guide](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/migrating-to-breakpoints.html) is also available.

#### New list rows and convenience functionality

Another improvement in the new libadwaita version is the introduction of a number of new types of list rows:

* [AdwSwitchRow](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.SwitchRow.html) is a row with a title, subtitle, and a switch.
* [AdwSpinRow](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.SpinRow.html) is a row which provides a spinbutton, with an editable number and add/substract buttons.
* The [.property style class](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/boxed-lists.html#property-rows) can be used with `AdwActionRow` and `AdwExpanderRow`, when the row is just displaying information.

Other enhancements in the new libadwaita release include:

* [show_about_window_from_appdata](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/func.show_about_window_from_appdata.html), a new convenience function which allows creating an [AdwAboutWindow](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.AboutWindow.html) from [AppStream data](https://www.freedesktop.org/software/appstream/docs/).
* A new description property in [AdwPreferencesPage](https://gnome.pages.gitlab.gnome.org/libadwaita/doc/1-latest/class.PreferencesPage.html), for showing text at the top of the page.

A [blog post is available](https://blogs.gnome.org/alicem/2023/09/15/libadwaita-1-4/) with more details about the libadwaita 1.4 release.

### GTK

GNOME 45 is accompanied by GTK 4.12. Improvements in this version include:

* [GtkListView](https://docs.gtk.org/gtk4/class.ListView.html) and [GtkGridView](https://docs.gtk.org/gtk4/class.GridView.html) have a new `scroll_to` method, for scrolling to a specific position. This allows scrolling without using actions, and gives control over which row or cell should be focused.
* Text rendering now uses the window scaling factor to control sub-pixel positioning of glyphs with newer versions of Cairo.
* Experimental support has been added for fractional scaling on Wayland. You can try it out with `GDK_DEBUG=gl-fractional`.

Rendering has been a focus of development effort for GTK 4.12, with the following improvements:

* Enhanced texture handling in the OpenGL renderer:
   * Performance has been improved by avoiding re-uploading of textures whenever possible.
   * Additional texture formats are now supported.
   * [GdkGLTextureBuilder](https://docs.gtk.org/gdk4/class.GLTextureBuilder.html) is a new builder which gives control over the creation of GL textures.
* The [GtkGlArea](https://docs.gtk.org/gtk4/class.GLArea.html) widget has a new [set_allowed_apis](https://docs.gtk.org/gtk4/method.GLArea.set_allowed_apis.html) method, for choosing whether OpenGL or OpenGL ES can be used. This replaces [get_use_es](https://docs.gtk.org/gtk4/method.GLArea.get_use_es.html) and [set_use_es](https://docs.gtk.org/gtk4/method.GLArea.set_use_es.html), which have been deprecated.
* Many improvements have also been made to the experimental Vulkan renderer.

GTK 4.12 also comes with improved accessibility:

* `accessible-label` and `accessible-description` now fully conform to the [WAI-ARIA specification](https://www.w3.org/WAI/standards-guidelines/aria/).
* Accessibility information is now correctly exposed when there are hidden private widgets in a widget tree.
* GTK inspector has a new accessibility overlay that displays missing labels and descriptions.

More can be read about these changes on the [GTK development blog](https://blog.gtk.org/2023/06/21/evolving-accessibility/).

## GLib

GNOME 45 is accompanied by GLib 2.78. Changes for this release include:

* File copying performance is improved by using the kernel's [`copy_file_range()`](https://man7.org/linux/man-pages/man2/copy_file_range.2.html), when it is available.
* GLib has acquired an internal list of pending GTasks, to allow more effective debugging using `gdb`. This can be used calling `print g_task_print_alive_tasks()` in `gdb`.
* The `GKeyFile` parser for [desktop entry files](https://specifications.freedesktop.org/desktop-entry-spec/latest/) has been improved, with comments being handled more reliably. This change may cause existing code relying on undefined behavior to break - please [file issues](https://gitlab.gnome.org/GNOME/glib/-/issues/new) if that is the case. (Reminder: GLib does not guarantee bug-for-bug backward compatibility.)
* Portability fixes on macOS, Android, and Windows.

### Libspelling

[Libspelling](https://gitlab.gnome.org/chergert/libspelling) is a new spell checking library for GTK 4. There is a [short post about it on Christian Hergert's blog](https://blogs.gnome.org/chergert/2023/07/12/spellchecking-for-gtk-4/).

### Libpeas

GNOME 45 comes with libpeas 2.0. The new version breaks ABI compatibility with previous versions. The main new feature is support for JavaScript plugins, using GJS. 

### Libdex

[Libdex](https://gitlab.gnome.org/GNOME/libdex) is a deferred execution library for GLib-based applications, which uses futures. GNOME 45 comes with version 0.4.0, which has a range of improvements, including:

 * Support for performing asynchronious I/O on traditional file-descriptors.
 * New GIO abstractions for `GDBusConnection` and `GSubprocess`.
 * Improved GObject Introspection integration.

<p markdown="1" class="full">
![Devel Docs](libmks.svg)
</p>

### Libmks

[Libmks](https://gitlab.gnome.org/chergert/libmks/) is a new library which provides a “Mouse, Keyboard, and Screen” implementation for QEMU, utilizing the D-Bus display device. It supports importing DMA-BUF into `GdkTexture` with damages for more efficient rendering as part of a GTK 4-based application.

## Language Bindings

Finally, GNOME 45 includes a range of improvements to the support for different programming languages.

### GNOME JavaScript (GJS)

GNOME 45 comes with GJS 1.78, which comes with a host of improvements:

* [GJS](https://gjs.guide/) has been updated to SpiderMonkey 115. This includes the following new APIs:
   * Arrays and typed arrays have gained `findLast()` and `findLastIndex()` methods, which act like `find()` and `findIndex()` respectively, but start searching at the end of the array.
   * Arrays and typed arrays have gained the `with()` method, which returns a copy of the array with one element replaced.
   * Arrays and typed arrays have gained `toReversed()`, `toSorted()`, and `toSpliced()` methods, which act like `reverse()`, `sort()`, and `splice()` respectively, but return a copy of the array instead of modifying it in-place.
   * The `Array.fromAsync()` static method acts like `Array.from()` but with async iterables, and returns a Promise that fulfills to the new Array.
* `console` formats `GLib.Error` properly. Consider using `console.error` instead of `logError`.
* `console` makes it easier to inspect objects by printing its constructor name. Consider using `console.log` instead `log`.
* `GObject.registerClass` accepts a string for the `Template` property.
* Automatically calls the garbage collector on low memory pressure.
* `gjs_context_run_in_realm()` is a new API for C programs embedding GJS. It allows using the SpiderMonkey API from the main realm where GJS code runs, and is intended for advanced use cases.

TypeScript support has also improved in GNOME, with a new [SDK extension](https://github.com/flathub/org.freedesktop.Sdk.Extension.typescript) and [Builder template](https://gitlab.gnome.org/BrainBlasted/gnome-typescript-template/).

### GNOME Rust

GNOME 45 comes with a new release of the [GTK Rust bindings](https://gtk-rs.org/), which feature the most recent API additions in the Rust the ecosystem. More details can be found in the [release notes](https://gtk-rs.org/blog/2023/08/28/new-release.html).

### GNOME Python

[PyGObject](https://pygobject.readthedocs.io/en/latest/) provides GNOME's official Python bindings, and comes with the following changes for GNOME 45:

* GTK2 support has been officially dropped.
* Support has been added for Python 3.12 and dropped for Python 3.7.
* The [PyGObject API](https://lazka.github.io/pgi-docs/) reference now covers GTK 4 and Libadwaita.
