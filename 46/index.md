---
layout: default
image: 46.png
languages: languages_46
urlprepend: "../"
---

# Introducing GNOME&nbsp;46, “Kathmandu”

**March 20, 2024**

The GNOME project is excited to present the latest GNOME release, version 46. This latest version is the result of 6 month's hard work by the GNOME community. Thank you to everyone who contributed!

GNOME 46 is code-named “Kathmandu”, in recognition of the amazing work done by the organizers of GNOME.Asia 2023.

Let’s dive in and explore what’s new in GNOME 46.

## Search Everywhere

Files comes with a **new global search feature** in GNOME 46. The feature is simple: activate it by clicking the new search button, or by using the <kbd>Ctrl</kbd>+<kbd>Shift</kbd>+<kbd>F</kbd> shortcut, then enter your query to search all your configured search locations.

Global search is a great way to jump directly into search, without having to think about where the items you want are located. The new feature also leverages GNOME's existing file search capabilities, including the ability to search the contents of files, and filter by file type and modification date.

Another great thing about global search is that it makes it possible to search multiple locations simultaneously, which can include locations that aren't in your home directory. To set this up, go to the newly refined search locations settings, and add the locations that you want to be included.

<picture class="full">
    <source media="(min-width: 800px) and (prefers-color-scheme: light)" srcset="search-everywhere-large.svg">
    <source media="(min-width: 800px) and (prefers-color-scheme: dark)" srcset="search-everywhere-large-dark.svg">
    <source media="(prefers-color-scheme: dark)" srcset="search-everywhere-dark.svg">
    <img src="search-everywhere.svg">
</picture>


## Enhanced Files App

In addition to global search, the Files app has had a serious upgrade for GNOME 46. Feedback about long-running file operations has been improved, with a new dynamic progress section at the bottom of the sidebar. This makes it possible to view the progress of individual operations, as well as see which operations are ongoing and which have finished.

<picture class="rounded">
    <source srcset="file-ops-screenshot-dark.png" media="(prefers-color-scheme: dark)">
    <img src="file-ops-screenshot.png">
</picture>

Significant code refactoring during the development of GNOME 46 has also delivered a welcome performance upgrade in the Files app. In the past, switching between list and grid view would involve a delay and content would progressively load on screen. In GNOME 46, this is gone: changing view is instantaneous, making for a faster and more frictionless experience.

<video nocontrols muted autoplay loop class="rounded">
  <p>Files 46: Improved performance when switching from grid and list views. Files 45 used to reload the view each time.</p>
  <source src="files-46.webm" type="video/webm">
  <source src="files-46.mp4" type="video/mp4">
</video>

Finally, Files in GNOME 46 also comes with a fantastic set of other smaller improvements:

* **Search for preferences:** you can now search within the Files preferences to locate specific settings.
* **Detailed date and time:** the Files preferences now include an option to show the date and time in a more comprehensive and consistent format.
* **Location entry on click:** quickly access the file location address bar by clicking on the file path area.
* **Starred favorites in grid view:** quickly identify and access your starred files with visual markers in grid view.
* **Improved network discovery:** more available networked devices now appear in the Other Locations view.

We encourage you to explore the new features in Files and experience a whole new level of file management efficiency!

## Upgraded Online Accounts, Now With OneDrive

GNOME's Online Accounts feature have had a major upgrade for GNOME 46. The biggest improvement is the new **support for Microsoft OneDrive**. Setup a Microsoft 365 account from the settings, and your OneDrive will appear in the Files sidebar, where it can be easily browsed and accessed alongside your local files and folders.

A collection of additional improvements to Online Accounts have also been made, thanks to funding from the [Sovereign Tech Fund](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/):

* The default web browser is now used when signing into accounts as part of account setup. This allows a **wider range of authentication methods** to be used, such as USB tokens.
* A **new WebDAV account type** has been added, providing a generic method for integrating online contacts, calendars and files into your GNOME experience.
* The Online Accounts **settings have also had a complete revamp**, and now have modern up to date designs.

A necessary consequence of these changes is that Online Accounts are no longer included as part of the initial system setup assistant.

## Remote Login with RDP

GNOME's remote desktop experience has been significantly enhanced for version 46, with the introduction of a new **dedicated remote login option**. This allows remotely connecting to a GNOME system which is not already in use. Connecting in this way means that the system's display can be configured from the remote side, resulting in a better experience for the remote user.

The new remote login feature means that GNOME systems can be used as fully fledged remote resources. It can be found in the Remote Desktop settings, which provide information on how to connect.

<picture>
    <source srcset="remote-screenshot-dark.png" media="(prefers-color-scheme: dark)">
    <img src="remote-screenshot.png">
</picture>

## Stepped-Up Settings

The Settings app has received a comprehensive update in GNOME 46.

### Navigation Improvements

The Settings app has been reorganized for GNOME 46, to make it **easier to navigate**. To this end, a new System section has been created, which contains preferences for *Region & Language*, *Date & Time*, *Users*, *Remote Desktop*, *Secure Shell*, and *About*. The Apps settings have also been consolidated, and now include the Default Apps and Removable Media settings.

These changes have the effect of reducing the total number of sections in the Settings app, giving you fewer places to browse and a faster path to the settings you need.

### New Touchpad Settings

The touchpad settings have been expanded for GNOME 46, with two new settings. The first, called **Secondary Click** provides configuration for how secondary clicks (often called "right click") are performed with a touchpad, and includes options to use either two fingers on the pad, or to click in the corner of the pad.

The second new setting allows the **Disable Touchpad While Typing** behavior to be turned off. Doing this allows touchpad movement to be combined with key presses, which can be necessary for some apps and games.

### Massive Polish Effort

The Settings app has also received a vast amount of polish for GNOME 46. This includes:

* improved settings descriptions and tooltips
* more keyboard mnemonics, to enhance keyboard navigation (hold down Alt to see these)
* extensive UI modernization and refinement
* additional options for compose key assignment
* faster loading of the appearance settings, as well as sharper previews
* additional fine-grained control when setting Wacom stylus pressure

Altogether, these improvements result in a Settings app which is clearer, easier to understand, and is fantastic to use.

## Accessibility Improvements

Accessibility has been a strong work theme for GNOME 46, with multiple aspects of accessibility seeing improvements. Much of this work has centered on the Orca screen reader:

* Under the hood, a **significant modernization effort** has been under way. This will result in improved performance and reliability, and will enable compatibility with Wayland and sandboxed apps in the future.
* A **new sleep mode** feature has been added. This much requested feature allows users to temporarily disable Orca, using the <kbd>Ctrl</kbd>+<kbd>Alt</kbd>+<kbd>Shift</kbd>+<kbd>Q</kbd> shortcut. Sleep mode is useful when using virtual machines that have their own screen readers, as well as self-voicing apps.
* New commands allow Orca to **report system status**, including battery status, CPU and memory usage.
* **Table navigation** has been much improved, with more apps being supported and new commands added, including toggle table navigation (<kbd>Orca</kbd>+<kbd>Shift</kbd>+<kbd>T</kbd>) and move to the final cell (<kbd>Orca</kbd>+<kbd>Alt</kbd>+<kbd>Shift</kbd>+<kbd>🡰</kbd>/<kbd>🡲</kbd>/<kbd>🡱</kbd>/<kbd>🡳</kbd>).
* Orca now has **experimental support for Spiel**, an exciting next generation speech synthesis API. Those who are interested in this feature are [invited to help test it](https://gitlab.gnome.org/GNOME/orca#experimental-features).

Orca hasn't been the only place where accessibility improvements have happened for GNOME 46. Other accessibility improvements include an **high contrast mode**  which is now more consistent and usable, and a **new setting to show on and off symbols in switches**.

Much of this accessibility work was funded by the [Sovereign Tech Fund](https://foundation.gnome.org/2023/11/09/gnome-recognized-as-public-interest-infrastructure/) and [Igalia](https://www.igalia.com/).

## System Enhancements

GNOME 46 contains many small improvements and polish changes to the core GNOME experience:

* **Beautiful fallback avatars:** if you don't specify an avatar for your user account, GNOME generates one for you. These fallback avatars have been refreshed for 46, and look fantastic. If you are upgrading from a previous version, clear out your old avatar to see the new default avatars in action.
* **Enhanced notifications:** with improved layouts, and a new header in each notification to show which app sent it. It is now also possible to expand notifications in the list, in order to use notification actions at a time that suits you.

![Enhanced Notifications](notifications-screenshot.png){:.rounded}

* **New app window shortcut:** apps which have been pinned to the dash could already be launched using <kbd>Super</kbd>+<kbd>&lt;Number&gt;</kbd>. For example, <kbd>Super</kbd>+<kbd>1</kbd> will launch the first app in the dash. GNOME 46 now includes additional shortcuts to allow launching a new window, by adding the <kbd>Ctrl</kbd> modifier, for example: <kbd>Super</kbd>+<kbd>Ctrl</kbd>+<kbd>&lt;Number&gt;</kbd> to launch a new window for the first app in the dash.
* **Improved on-screen keyboard:** with automatic capitalization and new keyboard layouts for entering phone numbers, email addresses, and URLs.
* **Tap to click is now enabled by default:** for a more intuitive touch experience.

## Better Apps

Many of GNOME's core apps have also been upgraded for GNOME 46. This includes:

* **Improved Software app:** indicators for Flathub apps whose developers have been verified, ensuring a trusted source for your software, have been improved this cycle. The Software app also has redesigned preferences, better error messages, and a new keyboard shortcuts window.
* **Various Maps enhancements:**
   * An updated OpenStreetMap point of interest editing experience, as well as other user interface and routing improvements.
   * New map style and support for dark mode
   * Information about multiple floors is now shown for points of interest that have them
   * Expanded public transit routing (now available for Norway)
* **Redesigned Extensions app:** a cleaner list makes it easier to scan and toggle your installed extensions, and the modernized design is also adaptive. It is now also possible to turn off extensions which have been automatically deactivated.
* **Polished Calendar app:** with improved visuals, modernized interfaces, and performance improvements.
* **Quick timers in Clocks:** when starting a timer, click a duration in the quick start section to quickly start a timer.
* **Smarter Contacts:** multiple VCard files can now be imported at the same time and, when importing new contacts, the confirmation dialog will also helpfully preview the names of the contacts to import.
* **Monitor disk usage:** Disks has gained a new resource graph for disk I/O.

<picture>
    <source srcset="software-screenshot-dark.png" media="(prefers-color-scheme: dark)">
    <img src="software-screenshot.png">
</picture>

## Improvements Under the Hood

The improvements in GNOME 46 are not just skin deep, and the new version comes with deep technical enhancements which result in a more performant and refined experience.

* **Performance and resource usage improvements**, including reduced memory usage for search, enhanced screen recording performance, and more intelligent use of system resources by the image viewer app. There have also been significant speed improvements in GNOME's terminal apps.
* **Security improvements** include enhanced protection from malware in the image viewer app and GNOME's search technologies.
* **Rendering improvements** include sharper app interfaces, crisper on-screen text, and clearer UIs when using fractional display scales. These rendering improvements are thanks to GTK's new renderers, and are found in GNOME apps which use the latest GTK version.
* **Variable refresh rates (VRR)** is a feature which can, under some circumstances, produce smoother video performance. This is included in GNOME 46 as an experimental feature, which needs to be enabled by entering the following from the command line using: ``gsettings set org.gnome.mutter experimental-features "['variable-refresh-rate']"``. Once enabled, a variable refresh rate can be set from the display settings.

## Welcome, Circle Friends!

[GNOME Circle](https://circle.gnome.org/) is a group of fantastic apps for GNOME, which the GNOME project promotes and supports. Since GNOME 45 was released, seven new apps have been added:

- [![Errands](io.github.mrvladus.List.svg){: .icon-dropshadow} <br>Errands](https://apps.gnome.org/List/)
- [![Letterpress](io.gitlab.gregorni.Letterpress.svg){: .icon-dropshadow} <br>Letterpress](https://apps.gnome.org/Letterpress/)
- [![Switcheroo](io.gitlab.adhami3310.Converter.svg){: .icon-dropshadow} <br>Switcheroo](https://apps.gnome.org/Converter/)
- [![Decibels](com.vixalien.decibels.svg){: .icon-dropshadow} <br>Decibels](https://apps.gnome.org/Decibels/)
- [![Fretboard](dev.bragefuglseth.Fretboard.svg){: .icon-dropshadow} <br>Fretboard](https://apps.gnome.org/Fretboard/)
- [![Graphs](se.sjoerd.Graphs.svg){: .icon-dropshadow} <br>Graphs](https://apps.gnome.org/Graphs/)
- [![Railway](de.schmidhuberj.DieBahn.svg){: .icon-dropshadow} <br>Railway](https://apps.gnome.org/DieBahn/)
{: .icon-grid}

Welcome to these new members of the GNOME community!

## Developer Experience

GNOME 46 includes new features and improvements for developers who are using the GNOME platform. Read the developers section to learn more.

<ul class="linkdiv">
  <li>
    <a href="developers/index.html"><span class="title">What's New for Developers</span>
    <span class="description">New features for those working with GNOME technologies.</span>
    </a>
  </li>
</ul>

## Getting GNOME 46

GNOME’s software is [Free Software](https://gnu.org/philosophy/free-sw.html): all [our code](https://gitlab.gnome.org/GNOME) is available for download and can be freely modified and redistributed according to the respective licenses. To install it, we recommend that you wait for the official packages provided by your vendor or distribution. Popular distributions will make GNOME 46 available very soon, and some already have development versions that include the new GNOME release. You can also try the [GNOME OS image](https://os.gnome.org/) as a virtual machine, using the [Boxes](https://apps.gnome.org/Boxes/) app.

## About GNOME

[The GNOME Project](https://www.gnome.org/about/) is an international community supported by a non-profit Foundation. We focus on user experience excellence and first-class internationalization and accessibility. GNOME is a free and open project: if you want to join us, [you can](https://welcome.gnome.org/).
