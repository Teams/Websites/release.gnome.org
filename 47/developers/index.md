---
layout: default
image: 47.png
urlprepend: "../../"
---

<picture class="full">
    <source srcset="developers-dark.svg" media="(prefers-color-scheme: dark)">
    <img src="developers.svg">
</picture>

# What's new for developers

GNOME 47 comes with plenty of new features and enhancements for those who use the GNOME platform. These include updates to GNOME's developer tools, improved libraries, and updated language bindings.

We encourage you to explore the [GNOME Developer website](https://developer.gnome.org/) for a comprehensive overview of the resources available to you. Get started building and contributing to the next generation of GNOME experiences!

## GTK

- **Default Vulkan Renderer:** GTK now uses the Vulkan renderer by default on systems with Vulkan support, enhancing graphical performance and rendering capabilities.

- **CSS Engine Enhancements:** Significant updates have been made to the CSS engine:
  - **CSS Variables:** Support for CSS variables has been added, allowing for more dynamic and reusable styles.
  - **New Color Features:** GTK has more color functionality, including relative colors, `color-mix`, and support for non-SRGB color spaces. Older APIs such as `@define-color` and `shade()` are now deprecated. For more details, visit [CSS Happenings](https://blogs.gnome.org/alicem/2024/06/07/css-happenings/).

- **Graphics Offloading Improvements:** The graphics offloading features in GTK and GStreamer have been enhanced. Applications like [Snapshot](https://flathub.org/apps/org.gnome.Snapshot), [Showtime](https://flathub.org/apps/org.gnome.Showtime), and [Epiphany](https://flathub.org/apps/org.gnome.Epiphany) now leverage these improvements for better performance.

- **Initial Color Management Support:** GTK has introduced initial support for color management, allowing it to communicate color space information for HDR content to the compositor. This ensures proper display of HDR images and videos. Additionally, the new `GdkColorState` object has been introduced to facilitate this functionality.

<picture class="full">
    <img src="gtk.svg">
</picture>

### Libadwaita

- **New Widgets Added:**
  - **AdwButtonRow:** A new widget designed for organizing buttons in a row. [Design guidelines](https://developer.gnome.org/hig/patterns/containers/boxed-lists.html#adding-buttons) are available for best practices.
  - **AdwSpinner and AdwSpinnerPaintable:** Added support for spinners to indicate loading or processing states.
  - **AdwBottomSheet:** This widget allows applications to use bottom sheets outside of dialogs and includes an optional bottom bar, which can be particularly useful for music player apps. [See it in action](https://thisweek.gnome.org/posts/2024/06/twig-152/32c286bf53b5bf2edc85b92cb39fcd7f4b6fa6bd1801318200277729280.webm).
  - **AdwMultiLayoutView:** Introduced for more flexible layout management.

- **Deprecated Old Dialog APIs:** Older dialog APIs have been deprecated in favor of newer implementations.

- **CSS Rework:** The CSS has been updated to align with recent GTK changes, such as replacing named colors with variables.

- **New Alert Dialog Style:** A refreshed style for alert dialogs has been introduced, improving visual consistency and user interaction.

## Shell

- **Wayland-Only Mutter Build:** After necessary cleanups in both Mutter and GNOME Shell, it's now possible to build Mutter as a Wayland-only compositor. GNOME Shell will automatically detect whether Mutter was built with or without X11 and Xwayland, and will adapt accordingly.

## Search

- **Renamed Components:** Two components of GNOME's search infrastructure have been renamed for clarity. The TinySPARQL database library and GNOME's filesystem indexer, LocalSearch, are now separate from the discontinued Tracker project.
- **Backward Compatibility:** The old Tracker APIs remain available for backward compatibility, so applications do not need to immediately transition to the new API. However, the `tracker3` command-line tool has been replaced by the `localsearch` command for terminal searches.
- **Improvements in TinySPARQL:** The TinySPARQL library has been renamed and updated with numerous fixes for robustness and standard compliance. A new browser-based query editor has also been introduced.
- **Enhancements in LocalSearch:** File unique IDs are now computed using a more reliable method, which helps prevent the repeated processing of files under certain conditions.

- **GNOME Shell Extensions:** For information on porting extensions, refer to the [porting guide](https://gjs.guide/extensions/upgrading/gnome-shell-47.html).

## Mutter: Wayland DRM Lease

This protocol is used by Wayland compositors which act as Direct Rendering Manager (DRM) masters to lease DRM resources to Wayland clients.

<div id="drm-lease"><noscript><img alt="gnome vr" src="gnomevr.webp"></noscript></div>

The compositor will advertise one wp_drm_lease_device_v1 global for each DRM node. Some time after a client binds to the wp_drm_lease_device_v1 global, the compositor will send a drm_fd event followed by zero, one or more connector events. After all currently available connectors have been sent, the compositor will send a wp_drm_lease_device_v1.done event.

<style type="text/css">
#drm-lease {
  aspect-ratio: 16/9;
  border-radius: 32px;
  overflow: hidden;
}
</style>
<script src="three.min.js"></script>
<script src="GLTFLoader.js"></script>
<script>
// Scene setup
const scene = new THREE.Scene();

// Camera setup
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
camera.position.z = 5;

// Renderer setup with alpha for transparent background
const renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
const canvasContainer = document.getElementById('drm-lease');
renderer.setSize(canvasContainer.clientWidth, canvasContainer.clientHeight);
renderer.outputEncoding = THREE.sRGBEncoding;
renderer.toneMapping = THREE.ACESFilmicToneMapping;
renderer.toneMappingExposure = 1.0;
canvasContainer.appendChild(renderer.domElement);

// Add lighting
const light = new THREE.HemisphereLight(0xffffff, 0x444444, 1);
light.position.set(0, 200, 0);
scene.add(light);

// GLTFLoader to load the 3D model
const loader = new THREE.GLTFLoader();
let model, mixer;

loader.load('vr.glb', (gltf) => {
  model = gltf.scene;
  scene.add(model);

  // Ensure textures are using the correct encoding
  model.traverse((node) => {
    if (node.isMesh) {
      node.material.map && (node.material.map.encoding = THREE.sRGBEncoding);
      node.material.emissiveMap && (node.material.emissiveMap.encoding = THREE.sRGBEncoding);
      node.material.needsUpdate = true;
    }
  });

  // Setup the animation mixer and play animations
  mixer = new THREE.AnimationMixer(model);
  gltf.animations.forEach((clip) => {
    const action = mixer.clipAction(clip);
    action.loop = THREE.LoopRepeat; // Set the animation to loop
    action.play(); // Play the animation
  });

}, undefined, (error) => {
  console.error(error);
});

// Variables to store cursor position
let mouseX = 0, mouseY = 0;

// Mouse move event listener
document.addEventListener('mousemove', (event) => {
  mouseX = (event.clientX / window.innerWidth) * 2 - 1;
  mouseY = -(event.clientY / window.innerHeight) * 2 + 1;
});

// ResizeObserver to handle dynamic resizing
const resizeObserver = new ResizeObserver(() => {
  const width = canvasContainer.clientWidth;
  const height = canvasContainer.clientHeight;
  renderer.setSize(width, height);
  camera.aspect = width / height;
  camera.updateProjectionMatrix();
});
resizeObserver.observe(canvasContainer);

// Animation loop with camera movement and mixer update
const animate = function () {
  requestAnimationFrame(animate);

  // Update the animation mixer on each frame
  const delta = clock.getDelta(); // Time since last frame
  if (mixer) {
    mixer.update(delta);
  }

  // Update camera based on cursor position
  camera.position.x = -mouseX * 2;
  camera.position.y = -mouseY * 2;
  camera.lookAt(scene.position);

  // Render the scene
  renderer.render(scene, camera);
};

// Clock for tracking time between frames (required for animation mixer)
const clock = new THREE.Clock();

animate();
</script>

## WebKitGTK

- **Sysprof Integration:** WebKit can now be profiled using Sysprof on Linux, marking the first web engine on the platform to integrate with Sysprof. This integration supports both WebKitGTK and WPE WebKit ports.
  - **Tracing Points:** The integration exposes existing tracing points within WebKit, including those related to JSC (JavaScriptCore), WebProcess, and more. There are plans to consider adding new tracing points in the future.
  - **Performance Benefits:** This development is a significant milestone for WebKit on Linux, as it enables potential performance enhancements, reduced resource usage, and improved overall efficiency.
  - **Visual Reference:** For more details, see the [visual reference](https://thisweek.gnome.org/posts/2024/07/twig-155/3e4cc632a1f604df93e13cb29703a3c5d3910f0d1809314527926288384.png).

## Orca

- **Accessibility Documentation:** A new document has been created to assist application developers in making their applications more accessible to screen reader users. You can view the document [here](https://gitlab.gnome.org/GNOME/orca/-/blob/main/README-APPLICATION-DEVELOPERS.md).

- **Improvements in Static Text Presentation:** The presentation of static text exposed via descriptions has been enhanced. This update is part of ongoing efforts to improve how text is presented to users relying on screen readers.

- **Formatting Strings Elimination:** The process of eliminating formatting strings for presentation generation has been completed, streamlining text formatting for screen reader users.

- **Braille Formatting Strings:** Work is underway to eliminate the use of formatting strings in Braille. This update aims to complete the transition from formatting strings, with a branch available for user testing. 

- **Global ARIA `posinset` and `setsize`:** The ARIA `posinset` and `setsize` attributes have been made global, allowing for better accessibility. This change also supports GTK developers in addressing issues related to the accessibility tree without needing to expose all objects. 

## PyGObject

- **Enhanced Documentation with Tutorials:** PyGObject, the Python bindings for GNOME platform libraries, has seen a major improvement in its documentation with the addition of new tutorials. These tutorials are derived from the PyGObject-Guide by Rafael Mardojai CM, which itself was based on the PyGObject-Tutorial by Sebastian Pölsterl. This effort involved contributions from the community and a relicense to LGPL. Explore the updated tutorials at [PyGObject Tutorials](https://pygobject.gnome.org/tutorials/).
