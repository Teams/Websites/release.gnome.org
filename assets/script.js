function lang_redirect(languages) {
  const stored_lang = localStorage.getItem('gnome_release_notes_lang');

  const lang = stored_lang ??
    navigator.languages
    .concat(navigator.languages.map(lang => lang.split('-')[0]))
    .filter(lang => languages.includes(lang))
    .shift();
  console.log(lang, document.documentElement.lang);

  if (lang && lang != document.documentElement.lang) {
    let url = window.location.href.split('/');
    let file = url.pop();
    if (!file) {
        file = 'index.html';
    }

    let new_file = file.split('.')[0];
    if (lang != 'en') {
        new_file += `.${lang}`;
    }
    new_file += '.html';

    url.push(new_file)
    const new_url = url.join('/')

    window.location.replace(new_url);
  }
}
